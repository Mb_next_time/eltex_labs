#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#define size 128

void display(char** mas, int q);								//Прототип для функции вывода на экран
void sort(char** mas, int q);									//Прототип для функции сортировки

int main() {
	
	char **str;
	int N;
	char buf[size];

	printf("\nЛаб.работа №2\n\n");

	printf("Введите кол-во строк: ");
	scanf("%d", &N);
	
	str = (char**)malloc(sizeof(char*)*N);						//Выделение памяти под массив указателей

	printf("Заполнение строк: \n\n");

	for(int i = 0; i < N; i++) {

		printf("%d строка: ", i+1);
		scanf(" %[^\n]", buf);	
		str[i] = (char*)malloc(sizeof(char)*strlen(buf));		//Выделение памяти под символьный массив
		strcpy(str[i], buf);
		str[i][strlen(buf)+1] = '\0';
	}

	printf("\n");
	sort(str, N);											    //Вызов функции для сортировки
	display(str, N);

	for (int i = 0; i < N; i++) {
        free(str[i]); 
    }

    free(str);

	return 0;
}

void display(char** mas, int q) {								//Функция для вывода на экрана

	printf("Вывод строк на экран:\n\n");

	for(int i = 0; i < q; i++) {
		printf("%s\n", mas[i]);
	}
}

void sort(char** mas, int q) {									//Функция для сортировки

	int number_1 = 0;
	int number_2 = 0;
	
	for(int i = 0; i < q-1; i++) {
		for(int j= 0; j < strlen(mas[i]); j++) {
			if((mas[i][j] >= '0') && (mas[i][j] <= '9')) {
				number_1++;
			}
		}
		for(int k = i+1; k < q; k++) {
			if(k > i+1) {
				for(int j= 0; j < strlen(mas[i]); j++) {
					if((mas[i][j] >= '0') && (mas[i][j] <= '9')) {
					number_1++;
					}
				}
			}
			for(int j= 0; j < strlen(mas[k]); j++) {
				if((mas[k][j] >= '0') && (mas[k][j] <= '9')) {
					number_2++;
				}
			}
			if(number_1 < number_2) {
				char* temp;
				temp = mas[i];
				mas[i] = mas[k];
				mas[k] = temp;
			}
			number_2 = 0;
			number_1 = 0;
			mas[i][strlen(mas[i])+1] = '\0';	
		}
	}
}