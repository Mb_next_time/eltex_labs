#include "Server.h"  

int main(int argc, char *argv[]) {

    pthread_t threadUDP_1, threadUDP_2, threadTCP_1, threadTCP_2;
    struct Prot UDP_1, UDP_2, TCP_1, TCP_2;

    if (argc < 4) {       
        fprintf(stderr,"Usage:  %s <Port for client №1> <Port for client №2> <IP Broadcast>\n", argv[0]);
        exit(1);
    }

    UDP_1.port = atoi(argv[1]);
    TCP_1.port = atoi(argv[1]);
    UDP_2.port = atoi(argv[2]);
    TCP_2.port = atoi(argv[2]);

    UDP_1.IP = argv[3];
    TCP_1.IP = argv[3];
    UDP_2.IP = argv[3];
    TCP_2.IP = argv[3];

    pthread_create(&threadUDP_1, NULL, UDP_Cl_1, &UDP_1);
    pthread_create(&threadTCP_1, NULL, TCP_Cl_1, &TCP_1);
    pthread_create(&threadUDP_2, NULL, UDP_Cl_2, &UDP_2);
    pthread_create(&threadTCP_2, NULL, TCP_Cl_2, &TCP_2);

    pthread_join(threadUDP_1, NULL);
    pthread_join(threadTCP_1, NULL);
    pthread_join(threadUDP_2, NULL);
    pthread_join(threadTCP_2, NULL);

    return 0;
}

void *UDP_Cl_1(void *threadArgs) {   
                 
    int sock;
    struct sockaddr_in broadcast; 
    struct Prot *point = (struct Prot*)threadArgs;
    

    sock = CreateUDPsocket(point->port, point->IP, &broadcast);

    int broadcastPermission = 1;

    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission)) < 0) {
        DieWithError("setsockopt() failed in UDP_Cl_1");
    } 

    for(;;) {
        if(N < QUEUE_MAX) {
            char mes[] = "I'am waiting messages.";
            unsigned int length; 

            length = strlen(mes);

            printf("The server has sended the message \"%s\" on %s\n",mes,point->IP);
            if (sendto(sock, mes, length, 0, (struct sockaddr *)&broadcast, sizeof(broadcast)) != length) {
                DieWithError("sendto() sent a different number of bytes than expected in UDP_Cl_1");
            }
            sleep(delay_UDP);   
        }
        else {
            printf("The queue is full.\n");
            sleep(delay_UDP);
        }
    }
}

void *UDP_Cl_2(void *threadArgs) {   
    
    int sock;
    struct sockaddr_in broadcast;
    struct Prot *point = (struct Prot*)threadArgs;
    
    sock = CreateUDPsocket(point->port, point->IP, &broadcast);

    int broadcastPermission = 1;

    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission)) < 0) {
        DieWithError("setsockopt() failed in UDP_Cl_1");
    }          

    for(;;) {
        if(N > QUEUE_MIN) {
            char mes[] = "The queue has message.";
            unsigned int length;

            length = strlen(mes);

            printf("The server has sended the message \"%s\" on %s\n",mes,point->IP);
            if (sendto(sock, mes, length, 0, (struct sockaddr *)&broadcast, sizeof(broadcast)) != length) {
                DieWithError("sendto() sent a different number of bytes than expected in UDP_Cl_2");
            }
            sleep(delay_UDP);   
        }
        else {
            printf("The queue is empty.\n");
            sleep(delay_UDP);
        }
    }
}

void* TCP_Cl_1(void *threadArgs) {
    
    int sock;
    struct Prot *point = (struct Prot*)threadArgs;

    sock = CreateTCPServerSocket(point->port);

    for(;;) {
        if(N < QUEUE_MAX) {
            
            int clntSock;
            
            clntSock = AcceptTCPConnection(sock);

            N++;
            recv(clntSock, &Q[N-1].length, sizeof(int), 0);
            Q[N-1].string = (char*)malloc(sizeof(char) * Q[N-1].length);
            recv(clntSock, Q[N-1].string, Q[N-1].length, 0);
            recv(clntSock, &Q[N-1].Time, sizeof(int), 0);
            printf("Messages in the queue: %d\n\n", N);
            close(clntSock);
        }
    }
}

void* TCP_Cl_2(void *threadArgs) {
    
    int sock;
    struct Prot *point = (struct Prot*)threadArgs;
    
    sock = CreateTCPServerSocket(point->port);

    for(;;) {

        int clntSock;       
        
        clntSock = AcceptTCPConnection(sock);

        send(clntSock, &Q[j].length, sizeof(int), 0);
        send(clntSock, Q[j].string, Q[j].length, 0);
        send(clntSock, &Q[j].Time, sizeof(int), 0);

        for(int k = j; k < N; k++) {
            Q[k]=Q[k+1];
        }
        N--;
        printf("Messages in the queue: %d\n\n", N);  
        close(clntSock);
    }
}



