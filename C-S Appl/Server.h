#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>     
#include <time.h>
#include <pthread.h>

#define QUEUE_MAX 10
#define QUEUE_MIN 0

int N = 0;
int j = 0;
int delay_UDP = 5;

struct Prot {
    int port;                      
    char *IP;
};

struct Queue {
    int length;
    char *string; 
    int Time;
} Q[QUEUE_MAX];

void *UDP_Cl_1(void* );
void *UDP_Cl_2(void* );
void *TCP_Cl_1(void* );
void *TCP_Cl_2(void* );
void DieWithError(char *errorMessage);  						// Error handling function 
int CreateTCPServerSocket(unsigned short port);					// Create TCP server socket
int CreateUDPsocket(int port, const void *IP, struct sockaddr_in* broadcast); 
int AcceptTCPConnection(int servSock);  						// Accept TCP connection request
