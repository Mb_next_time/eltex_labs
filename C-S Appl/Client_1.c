#include "Client_1.h" 

int main(int argc, char *argv[]) {
                            
    if (argc != 2) {
        fprintf(stderr,"Usage: %s <Port>\n", argv[0]);
        exit(1);
    }

    unsigned short port;

    port = atoi(argv[1]);  

    int sockUDP;
    struct sockaddr_in broadcastAddr;

    sockUDP = CreateUDPsocket(port, NULL, &broadcastAddr); 

    if (bind(sockUDP, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0) {
        DieWithError("bind() failed Client №1");
    }

    struct sockaddr_in fromAddr;
    unsigned int fromAddrsize;

    fromAddrsize = sizeof(fromAddr);

    for(;;) {
        char advice[SIZE_BUF];            
        int length_of_advice;

        if ((length_of_advice = recvfrom(sockUDP, advice, SIZE_BUF, 0, (struct sockaddr *)&fromAddr, &fromAddrsize)) < 0) {
            DieWithError("recvfrom() failed Client №1");
        }
        else {
            int sockTCP;
            struct sockaddr_in ServAddr;

            char* IP = inet_ntoa(fromAddr.sin_addr);

            advice[length_of_advice] = '\0';

            printf("The client №1 has accepted the message \"%s\" by %s(Server)\n",advice,IP);    /* Print the received string */

            sockTCP = CreateTCPsocketClient(port, IP); 

            srand(time(NULL));

            int length;
            char *string;
            int Time;

            length = rand() % MAX_LENGTH + MIN_LENGTH;
            send(sockTCP, &length, sizeof(int), 0);
            string = (char*)malloc(sizeof(char) * length);
            string = random_string(string, length);
            string[length+1] = '\0';
            send(sockTCP, string, length, 0);
            Time = rand() % MAX_DELAY + MIN_DELAY;
            send(sockTCP, &Time, sizeof(int), 0);
            printf("The client №1 has sended the message: %s\n", string);
            free(string);
            IP = NULL;
            
            int delay = Time;;
            close(sockTCP);
            sleep(delay);
        }
    }
}
