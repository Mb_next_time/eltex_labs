#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <signal.h>
#include <fcntl.h>

void display(int**, int, int, int);
int** filling_area(int**, int);
void directing(int, int*, int*);
void record(FILE* ,int, int, int);
int kill(pid_t pid, int sig);

int main() {
    int *mas_X;
    int *mas_Y;
    int *mas_L;
    int **playing_area;
    int* x = NULL;
    int *y = NULL;
    int status, stat;
    int count_players = 2;
    int df;
    FILE *source, *out;


    srand(getpid());
    const int size_of_area = 5;

    playing_area = (int**)malloc(sizeof(int)*size_of_area);

    for(int i = 0; i < size_of_area; i++) {
        playing_area[i] = (int*)malloc(sizeof(int)*size_of_area); 
    }

    playing_area = filling_area(playing_area, size_of_area);

    const int n = 2;
    // printf("Введите кол-во игроков: ");
    // scanf("%d", &n);
    pid_t pid[n];

    printf("Start game\n");
    int lives = 1;
    int position_x;
    int position_y;
    //int steps = 0; 
    source = fopen("data.txt", "w");
    if (source==NULL) {
        printf("Cannot Open\n");
        exit(1);
    }
    out = fopen("data.txt", "r");
    if (out==NULL) {
        printf("Cannot Open\n");
        exit(1);
    }
    mas_X = (int*)malloc(sizeof(int)*n);
    mas_Y = (int*)malloc(sizeof(int)*n);
    mas_L = (int*)malloc(sizeof(int)*n);

    int steps = 0;
    while(count_players > 0) {
        for(int i = 0; i < n; i++) {
            pid[i] = fork();
            if( pid[i] == 0) {
                x = &position_x;
                y = &position_y;
                if (steps == 0) {
                    srand(getpid());
                    position_x=(rand()%size_of_area);
                    position_y=(rand()%size_of_area);
                    lives = 1;
                    record(source, lives, position_x, position_y);
                    exit(2);
                }
                else {
                    position_x = mas_X[i];
                    position_y = mas_Y[i];
                    lives = mas_L[i];
                }
                if(lives <= 0) {
                    exit(3);
                }
                else {
                    printf("Player %d\n", i+1);                                                                  
                    printf("Steps - %d\n", steps); 
                    // printf("Mas_x: %d\n", mas_X[i]);
                    // printf("Mas_y: %d\n", mas_Y[i]);
                    // printf("Mas_l: %d\n", mas_L[i]);
                    printf("Position: x=%d;y=%d\n", position_x,position_y);
                    playing_area[position_x][position_y] = 0;
                    display(playing_area, size_of_area,position_x,position_y);
                    printf("Lives: %d\n",lives);
                    /*Start code game */                
                    int k = rand()%4;
                    directing(k, x, y);
                    if ((position_x < 0) || (position_x >= size_of_area) || (position_y < 0) || (position_y >= size_of_area)) {
                        printf("x=%d; y=%d\n",position_x,position_y);
                        display(playing_area, size_of_area,position_x,position_y);
                        lives = 0;
                        record(source, lives, position_x, position_y);
                        exit(1);
                    }
                    if(playing_area[position_x][position_y] == 1) {
                        printf("x=%d; y=%d\n",position_x,position_y);
                        lives++;
                        playing_area[position_x][position_y] = 0;
                        display(playing_area, size_of_area,position_x,position_y);
                        printf("Lives: %d\n",lives);
                        record(source, lives, position_x, position_y);
                        exit(2);
                    }
                    else {
                        if (playing_area[position_x][position_y] == -1) {
                            printf("x=%d; y=%d\n",position_x,position_y);
                            lives--;
                            playing_area[position_x][position_y] = 0;
                            display(playing_area, size_of_area,position_x,position_y);
                            printf("Lives: %d\n",lives);
                            record(source, lives, position_x, position_y);
                            if(lives <= 0) {
                                exit(0);
                            }
                            else {
                                exit(2);
                            }
                        }
                        else {
                            if (playing_area[position_x][position_y] == 0) {
                                printf("x=%d; y=%d\n",position_x,position_y);
                                playing_area[position_x][position_y] = 0;
                                display(playing_area, size_of_area,position_x,position_y);
                                printf("Lives: %d\n",lives);
                                record(source, lives, position_x, position_y);
                                exit(2);
                            }
                        }
                    }
                }
            }
            sleep(5);
            status = waitpid(pid[i], &stat, WIFEXITED(status));
            //printf("Status - %d\n", status);
            if (pid[i] == status) {
                //printf("Stat - %d\n", stat);
                if(WEXITSTATUS(stat) == 0) {
                    printf("Player %d is dead\n", i+1);
                    fscanf(out, "%d", &lives);
                    mas_L[i] = lives;
                    fscanf(out, "%d", &position_x);
                    mas_X[i] = position_x;
                    fscanf(out, "%d", &position_y);
                    mas_Y[i] = position_y;
                    playing_area[position_x][position_y] = 0;
                    count_players--;
                }
                else {
                    if(WEXITSTATUS(stat) == 1) {
                        printf("Player %d out area\n", i+1);
                        fscanf(out, "%d", &lives);
                        mas_L[i] = lives;
                        //printf("Mas_l: %d\n", mas_L[i]); 
                        fscanf(out, "%d", &position_x);
                        mas_X[i] = position_x;
                        //printf("Mas_x: %d\n", mas_X[i]);  
                        fscanf(out, "%d", &position_y);
                        mas_Y[i] = position_y;
                        count_players--;
                    }
                    else {
                        if( WEXITSTATUS(stat) == 2 ) {
                            printf("Player %d is alive\n", i+1);
                            fscanf(out, "%d", &lives);
                            mas_L[i] = lives;
                            //printf("Mas_l: %d\n", mas_L[i]); 
                            fscanf(out, "%d", &position_x);
                            mas_X[i] = position_x;
                            //printf("Mas_x: %d\n", mas_X[i]);  
                            fscanf(out, "%d", &position_y);
                            mas_Y[i] = position_y;
                            //printf("Mas_y: %d\n", mas_Y[i]); 
                            playing_area[position_x][position_y] = 0;
                            if ((mas_X[i] == mas_X[i-1]) && (mas_Y[i] == mas_Y[i-1])) {              // Fight for 2 players                             
                                printf("Fight!!!\n");
                                if(mas_L[i] > mas_L[i-1]) {
                                    printf("Player %d win!!!\n", i+1);
                                    count_players--;
                                    printf("Player %d is dead\n", i);
                                    lives = 0;
                                    mas_L[i] = lives;
                                }
                                else {
                                    if (mas_L[i] < mas_L[i-1]) {
                                        printf("Player %d win!!!\n", i);
                                        printf("Player %d is dead\n", i+1);
                                        count_players--;
                                        lives = 0;
                                        mas_L[i-1] = lives;
                                    }
                                    else {
                                        printf("No sides!\n");
                                    }
                                }
                            }
                        }
                        else {
                            if (WEXITSTATUS(stat) == 3) {
                                kill(pid[i],SIGKILL);
                            }
                        }
                    }
                }
            }
            if(count_players <= 0) {
                printf("Finish game\n");
                free(mas_X);
                free(mas_Y);
                free(mas_L);
                fclose(out);
                for(int i =0; i < size_of_area; i++) {
                    free(playing_area[i]);
                }
                free(playing_area);
                exit(0);
            }                                                                       
        }
        steps++; 
    }
}

int** filling_area(int** mas, int size) {
    int filling[] = {-1, 0, 1};

    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            int k = rand()%3;
            mas[i][j] = filling[k];
        }
    }

    return mas;
}

void display(int** mas, int size, int position_x, int position_y) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            if((i == position_x) && (j == position_y)) {
                printf("[%2d] ", mas[i][j]);
            }
            else {
                printf("%4d ", mas[i][j]);
            }
        }
        printf("\n");
    }
}

void directing(int k, int* x, int* y) {
    if(k == 0) {
        *x-=1;
        printf("Moving up\n");
    }
    if(k == 1) {
        *x+=1;
        printf("Moving down\n");
    }
    if(k == 2) {
        *y+=1;
        printf("Moving right\n");
    }
    if(k == 3) {
        *y-=1;
        printf("Moving left\n");
    }
}

void record(FILE* fp,int L, int X, int Y) {
    fprintf(fp, "%d\n", L);
    fprintf(fp, "%d\n", X);
    fprintf(fp, "%d\n", Y);
    fseek (fp, 0, SEEK_END);
    flock(fileno(fp), 3);
}
