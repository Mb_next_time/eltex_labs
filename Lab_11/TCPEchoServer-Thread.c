#include "TCPEchoServer.h"  /* TCP echo server includes */
#include <pthread.h>        /* for POSIX threads */

#define RCVBUFSIZE 512   /* Size of receive buffer */

void *ThreadMain(void *arg);            /* Main program of a thread */

/* Structure of arguments to pass to client thread */
struct ThreadArgs {
    int clntSock;                      /* Socket descriptor for client */
    int data;
    char string[RCVBUFSIZE];
    char namefile[RCVBUFSIZE];
};

int main(int argc, char *argv[]) {
    int servSock;                    /* Socket descriptor for server */
    int clntSock;                    /* Socket descriptor for client */
    unsigned short echoServPort;     /* Server port */
    int N;
    pthread_t *threadID;              /* Thread ID from pthread_create() */
    struct ThreadArgs *threadArgs;   /* Pointer to argument structure for thread */

    
    int number;
    int echoStringLen;
    char echoBuffer[RCVBUFSIZE];

    if (argc != 2) {                             /* Test for correct number of arguments */
        fprintf(stderr,"Usage:  %s <SERVER PORT>\n", argv[0]);
        exit(1);
    }

    echoServPort = atoi(argv[1]);  /* First arg:  local port */

    servSock = CreateTCPServerSocket(echoServPort);

    for (;;) {                                              /* run forever */
        clntSock = AcceptTCPConnection(servSock);

        recv(clntSock, &N, sizeof(int), 0);
        // printf("N - %d\n", N);
        /* Create separate memory for client argument */
        if ((threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs)*N)) == NULL) {
            DieWithError("malloc() failed");
        }
        
        for(int i = 0; i < N; i++) {
            threadArgs[i].clntSock = clntSock;
            recv(clntSock, &number, sizeof(int), 0);                                
            threadArgs[i].data = number;
            recv(clntSock, &echoStringLen, sizeof(int), 0);
            recv(clntSock, echoBuffer, echoStringLen, 0);
            echoBuffer[echoStringLen] = '\0';
            printf("Название подстроки: %s\n", echoBuffer);
            strcpy(threadArgs[i].string, echoBuffer);
            recv(clntSock, &echoStringLen, sizeof(int), 0);
            recv(clntSock, echoBuffer, echoStringLen, 0);
            echoBuffer[echoStringLen] = '\0';
            strcpy(threadArgs[i].namefile,echoBuffer);
            printf("Название файла: %s\n", threadArgs[i].namefile);
        }
        
        threadID = (pthread_t*)malloc(sizeof(pthread_t)*N);
        /* Create client thread */
        for(int i = 0; i < N; i++) {
            pthread_create(&threadID[i], NULL, ThreadMain, &threadArgs[i]);
        }
        for (int i = 0; i < N; i++){
            pthread_join(threadID[i], NULL);
        }

        free(threadArgs);
        free(threadID);
    }
    /* NOT REACHED */
}

void *ThreadMain(void *threadArgs) {   

    char buf[RCVBUFSIZE]; 
    FILE *source;
    int count = 1;
    int lim = 0;
    struct ThreadArgs *point = (struct ThreadArgs*)threadArgs;

    printf("Проверяемый файл: %s\n", point->namefile);

    if((source = fopen(point->namefile, "r")) == NULL) {
        printf("Файла не существует или вы ошиблись в названии файла.\n");
        return 0;
    }

    while (fgets(buf, RCVBUFSIZE, source) != NULL) {
        if(point->data == count) {
            break;
        }
        count++;
    }

    printf("Строка: %s", buf);

    int check = 0;

    for(int i = 0; i < strlen(buf); i++) {
        int lim_str = strlen(point->string);
        int count_str = 0;
        int k = 0;
        
        while(count_str <= lim_str) {
            if(point->string[k] == buf[i]) {
                i++;
                k++;
                count_str++;
                if(count_str == lim_str) {
                    check++;
                }
            }
            else {
                count_str = 0;
                k = 0;
                break;
            }
        }
    }

    if(check == 0) {
        printf("Подстрока %s не найдена.\n\n", point->string);
    }
    else {
        printf("Подстрока %s найдена.\n\n", point->string);
    }
    
    fclose(source);

    return 0;
}
