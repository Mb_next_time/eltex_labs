#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define RCVBUFSIZE 512   /* Size of receive buffer */

void DieWithError(char *errorMessage);  /* Error handling function */

int main(int argc, char *argv[]) {
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    unsigned short echoServPort;     /* Echo server port */
    char *servIP;                    /* Server IP address (dotted quad) */
    char *echoString;                /* String to send to echo server */
    char echoBuffer[RCVBUFSIZE];     /* Buffer for echo string */
    unsigned int echoStringLen;      /* Length of string to echo */
    int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv() 
                                        and total bytes read */

    char *string;
    char buf[256];
    int N;
    int number;

    if ((argc < 3) || (argc > 4)) {                  /* Test for correct number of arguments */
       fprintf(stderr, "Usage: %s <Server IP> <Echo number_of_string> [<Echo Port>]\n", argv[0]);
       exit(1);
    }

    servIP = argv[1];             /* First arg: server IP address (dotted quad) */
    echoString = argv[2];         /* Second arg: string to echo */

    if (argc == 4) {
        echoServPort = atoi(argv[3]); /* Use given port, if any */
    }
    else {
        echoServPort = 7;  /* 7 is the well-known port for the echo service */
    }

    /* Create a reliable, stream socket using TCP */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        DieWithError("socket() failed");
    }

    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
    echoServAddr.sin_family      = AF_INET;             /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    echoServAddr.sin_port        = htons(echoServPort); /* Server port */

    /* Establish the connection to the echo server */
    if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) {
        DieWithError("connect() failed");
    }

    printf("Введите кол-во строк для обработки: ");
    scanf("%d", &N);
    send(sock, &N, sizeof(int), 0);
    
    for(int i = 0; i < N; i++) {
        printf("Введите номер строки для вывода из файла: ");
        scanf("%d", &number);
        send(sock, &number, sizeof(int), 0);
        printf("Введите подстроку для поиска: ");
        scanf(" %[^\n]", buf);
        string = (char*)malloc(sizeof(char)*strlen(buf));
        strcpy(string, buf);
        printf("Подстрока для поиска: %s\n", string);
        echoStringLen = strlen(string);
        send(sock, &echoStringLen, sizeof(int), 0);
        send(sock, string, echoStringLen, 0);
        printf("Файл для обработки - %s\n", echoString);
        echoStringLen = strlen(echoString);
        send(sock, &echoStringLen, sizeof(int), 0);
        if (send(sock, echoString, echoStringLen, 0) != echoStringLen)
            DieWithError("send() sent a different number of bytes than expected");
    }
   
    close(sock);
    exit(0);
}