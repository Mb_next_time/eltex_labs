int lengthString(char *str) {

	int i = 0;

	while (str[i] != '\0') {
		i++;
	}

	return i;
}

char* splice(char *str1, char *str2) {

	int position = lengthString(str1);
	int i;
	int j;
	for (i = position, j = 0; i < lengthString(str1) + lengthString(str2), j < lengthString(str2); i++, j++) {
		str1[i] = str2[j];
	}

	return str1;
}

char* break_str(char* str1, char* str2, int position_for_break) {

	int end;
	int i;
	int j;
	for (i = 0, j = position_for_break-1; i < position_for_break-1, j < lengthString(str1); i++, j++) {
		str2[i] = str1[j];
		end = i+1;
	}
	str1[position_for_break-1] = '\0';
	str2[end] = '\0';

	return str1;
}
