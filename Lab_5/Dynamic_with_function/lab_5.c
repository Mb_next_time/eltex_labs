#include <stdio.h>
#include <dlfcn.h>

int main(int argc, char* argv[]){

	void *ext_library;														// хандлер внешней библиотеки
	char s1[128] = "string1+?231";											// значение для теста
	char s2[128] = "string2";
	// int (*func1)(char* str);													// переменная для хранения адреса функции
	char* (*func)(char* str1, char* str2);
	// char* (*func3)(char* str1, char* str2);

	//загрузка библиотеки
	ext_library = dlopen("/home/gameover/Work/Lab_5/Dynamic_with_function/libdyn.so",RTLD_LAZY);
	if (!ext_library){
		//если ошибка, то вывести ее на экран
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		return 1;
	};

	//загружаем из библиотеки требуемую процедуру
	func = dlsym(ext_library, argv[1]);	
	// func2 = dlsym(ext_library, argv[1]);
	// func3 = dlsym(ext_library, argv[1]);

	//выводим результат работы процедуры
	// printf("%s = %d\n",argv[1],(*func)(s1));
	printf("%s = %s\n",argv[1],(*func)(s1, s2));
	// printf("%s = %s\n",argv[1],(*func)(s1, s2));
	// //закрываем библиотеку
	dlclose(ext_library);
};