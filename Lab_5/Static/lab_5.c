#include <stdio.h>
extern int lengthString(char *str);
extern char* splice(char *str1, char *str2);
extern char* break_str(char* str1, char* str2, int position_for_break);

int main() {

	char s1[128] = "string1";
	char s2[128] = "+string2";

	printf("String - 1: %s\n", s1);
	splice(s1, s2);
	printf("После объединения: \n");
	printf("String - 1: %s\n", s1);
	break_str(s1, s2, 8);
	printf("После разбивки: \n");
	printf("String - 1: %s\n", s1);
	printf("String - 2: %s\n", s2);

	return 0;
}