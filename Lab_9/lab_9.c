#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>
#include <string.h>
#include <wait.h>
#include <signal.h>
#include <fcntl.h>

void display(int**, int, int, int);
int** filling_area(int**, int);
void directing(int, int*, int*);
int calculate_direct(int , int, int);
int kill(pid_t pid, int sig);

union semun {
	int val;                  
	struct semid_ds *buf;     
	unsigned short *array;                          
	struct seminfo *__buf;    
};

struct DATA {
	int x;
	int y;
	int status_scout;
};

int main() {

	int **area;
	int *x = NULL;
	int *y = NULL;
	int status, stat;
	int count_scout = 2;
	const int size_of_area = 5;
	int count_of_goals = 0;
	pid_t pid[count_scout];
	int n = count_scout;
	int *mas_X;
	int *mas_Y;
	int *mas_St;
	int *mas_direct;
	srand(getpid());

	area = (int**)malloc(sizeof(int)*size_of_area);

	for(int i = 0; i < size_of_area; i++) {
		area[i] = (int*)malloc(sizeof(int)*size_of_area); 
	}

	area = filling_area(area, size_of_area);

	int shmid;
	key_t key = 69;
	struct DATA *shm;
	struct DATA MIS;
	int semid;
	union semun arg;
	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};		//освобождение ресурса

	x = &MIS.x;
	y = &MIS.y;

	semid = semget(key, 1, 0666 | IPC_CREAT);

	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);

	shmid = shmget(key, sizeof(struct DATA*), IPC_CREAT | 0666);
	
	mas_X = (int*)malloc(sizeof(int)*n);
	mas_Y = (int*)malloc(sizeof(int)*n);
	mas_St = (int*)malloc(sizeof(int)*n);
	mas_direct = (int*)malloc(sizeof(int)*n);

	for(int i = 0; i < n; i++) {
		mas_X[i] = (rand()%size_of_area);
		mas_Y[i] = (rand()%size_of_area);
		mas_St[i] = 1;
		mas_direct[i] = calculate_direct(mas_X[i], mas_Y[i], size_of_area);
	}


	while( count_scout > 0 ) {
		for(int i = 0; i < n; i++) {
			pid[i] = fork();
			if(pid[i] == 0) {
				printf("Scout - %d\n", i);
				MIS.x = mas_X[i];
				MIS.y = mas_Y[i];
				MIS.status_scout = mas_St[i];
				printf("X - %d, Y - %d\n", MIS.x, MIS.y);
				if(MIS.status_scout == 0) {
					exit(4);
				}
				shm = (struct DATA*)shmat(shmid, NULL, 0);
				if ((MIS.x < 0) || (MIS.x >= size_of_area) || (MIS.y < 0) || (MIS.y >= size_of_area)) {
					printf("Scout out area.\n");
					semop(semid, &lock_res, 1);			
					*shm = MIS;
					semop(semid, &rel_res, 1);
					exit(1);
				}
				if(area[MIS.x][MIS.y] == 1) {
					printf("Goal finded.\n");
					semop(semid, &lock_res, 1);			
					*shm = MIS;
					semop(semid, &rel_res, 1);
					exit(2);
				}
				if ( area[MIS.x][MIS.y] == 0) {
					semop(semid, &lock_res, 1);
					*shm = MIS;
					semop(semid, &rel_res, 1);
					exit(3);
				}
			}
			sleep(2);
			status = waitpid(pid[i], &stat, WIFEXITED(status));
			if (pid[i] == status) {
				printf("stat: %d\n", WEXITSTATUS(stat));
				if (WEXITSTATUS(stat) == 1) {
					MIS.x = shm->x;
					MIS.y = shm->y;
					display(area, size_of_area,MIS.x, MIS.y);
					MIS.status_scout = shm->status_scout;
					directing(mas_direct[i],x,y);
					mas_X[i] = MIS.x;
					mas_Y[i] = MIS.y;
					mas_St[i] = 0;
					count_scout--;
				}
				if (WEXITSTATUS(stat) == 2) {
					shm = (struct DATA*)shmat(shmid, NULL, 0);
					MIS.x = shm->x;
					MIS.y = shm->y;
					display(area, size_of_area,MIS.x, MIS.y);
					MIS.status_scout = shm->status_scout;
					count_of_goals++;
					directing(mas_direct[i],x,y);
					mas_X[i] = MIS.x;
					mas_Y[i] = MIS.y;
					mas_St[i] = 1;
				}
				if (WEXITSTATUS(stat) == 3) {
					shm = (struct DATA*)shmat(shmid, NULL, 0);
					MIS.x = shm->x;
					MIS.y = shm->y;
					display(area, size_of_area,MIS.x, MIS.y);
					MIS.status_scout = shm->status_scout;
					directing(mas_direct[i],x,y);
					mas_X[i] = MIS.x;
					mas_Y[i] = MIS.y;
					mas_St[i] = 1;
				}
				if (WEXITSTATUS(stat) == 4) {
					kill(pid[i],SIGKILL);
				}
			}
		}
		if(count_scout <= 0) {
			printf("Mission complited.\n");
			printf("Кол-во целей: %d\n", count_of_goals);
			free(mas_X);
			free(mas_Y);
			for(int i =0; i < size_of_area; i++) {
				free(area[i]);
			}
			free(area);
			semctl(semid, 0, IPC_RMID);
			exit(0);
		} 
	}
}

int** filling_area(int** mas, int size) {
	int filling[] = {0,1};

	for(int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			int k = rand()%2;
			mas[i][j] = filling[k];
		}
	}

	return mas;
}

void display(int** mas, int size, int position_x, int position_y) {
	for(int i = 0; i < size; i++) {
		for(int j = 0; j < size; j++) {
			if((i == position_x) && (j == position_y)) {
				printf("[%2d] ", mas[i][j]);
			}
			else {
				printf("%4d ", mas[i][j]);
			}
		}
		printf("\n");
	}
}

int calculate_direct(int x, int y, int s) {

	int value[4];
	int max = 0;
	int index;

	value[0] = (s -1) - x;						// Down
	value[1] = x + 0;							// Up
	value[2] = (s-1) - y;						// Right
	value[3] = y - 0;							// Left

	for(int i = 0; i < 3; i++) {
		if (value[i] >= value[i+1]) {
			if (value[i] > max) {
				max = value[i];
				index = i;
			}
		}
		else {
			if (value[i+1] > max) {
				max = value[i+1];
				index = i+1;
			}
		}
	}
	if(index == 0) {
		return 1;
	}
	if(index == 1) {
		return 0;
	}
	if(index == 2) {
		return 2;
	}
	if(index == 3) {
		return 3;
	}
}

void directing(int k, int* x, int* y) {

	if(k == 0) {
		*x-=1;
		printf("Moving up\n");
	}
	if(k == 1) {
		*x+=1;
		printf("Moving down\n");
	}
	if(k == 2) {
		*y+=1;
		printf("Moving right\n");
	}
	if(k == 3) {
		*y-=1;
		printf("Moving left\n");
	}
}