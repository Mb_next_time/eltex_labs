#include "funcechoServ.h"

// unsigned short csum(unsigned short *buf, int nwords)
// {
//     unsigned long sum;
//     for(sum=0; nwords>0; nwords--)
//         sum += *buf++;
//     sum = (sum >> 16) + (sum &0xffff);
//     sum += (sum >> 16);
//     return (unsigned short)(~sum);
// }

int main() {

	/*Открытие raw-socket*/

	int sockfd;

	if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_UDP)) == -1) {
    	perror("socket");
	}

	/*Получение MAC-адреса */

	struct ifreq if_mac;																
	struct ifreq if_idx;									

	memset(&if_mac, 0, sizeof(struct ifreq));
  	strncpy(if_mac.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

  	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0) {		
    	perror("SIOCGIFHWADDR");							
  	}

	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0) {			
    	perror("SIOCGIFINDEX");
	}

	/* Получение IP-адрес */

	struct ifreq if_ip;

	memset(&if_ip, 0, sizeof(struct ifreq));
	strncpy(if_ip.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

	if (ioctl(sockfd, SIOCGIFADDR, &if_ip) < 0) {			
    	perror("SIOCGIFADDR");
	}

	int tx_len = 0;
	char sendbuf[BUF_SIZ];
	uint8_t macserver[] = {0x20, 0x1a, 0x06, 0x6e, 0x6f, 0xe4};
	char destIP[] = "192.168.1.2";

	struct ether_header *eh = (struct ether_header*)sendbuf;

	for(int i = 0; i < LENMAC; i++) {
		eh->ether_shost[i] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[i];
		eh->ether_dhost[i] = macserver[i];
	}
	
	eh->ether_type = htons(ETHER_TYPE);								
	tx_len += sizeof(struct ether_header);

	/* Конструирование IP-заголовка */

	struct iphdr *iph = (struct iphdr *) (sendbuf + sizeof(struct ether_header));
	/* IP Header */
	iph->ihl = 5;
	iph->version = 4;
	iph->tos = 16; // Low delay
	iph->id = htons(54321);
	iph->ttl = 64; // hops
	iph->protocol = 17; // UDP
	/* Source IP address, can be spoofed */
	iph->saddr = inet_addr(inet_ntoa(((struct sockaddr_in *)&if_ip.ifr_addr)->sin_addr));
	/* Destination IP address */
	// iph->daddr = inet_addr(inet_ntoa(((struct sockaddr_in *)&if_ip.ifr_addr)->sin_addr));
	iph->daddr = inet_addr(destIP);
	tx_len += sizeof(struct iphdr);

	/* Конструирование UDP-заголовка*/

	struct udphdr *udph = (struct udphdr *) (sendbuf + sizeof(struct iphdr) + sizeof(struct ether_header));

	/* UDP Header */
	int PortS = 8000;
	udph->source = htons(PortS);
	int PortD = 8001;
	udph->dest = htons(PortD);
	udph->check = 0; // skip
	tx_len += sizeof(struct udphdr);

	/*Заполнение поля Data */

	sendbuf[tx_len++] = 't';
	sendbuf[tx_len++] = 'e';
	sendbuf[tx_len++] = 's';
	sendbuf[tx_len++] = 't';
	
	/* Length of UDP payload and header */
	udph->len = htons(tx_len - sizeof(struct ether_header) - sizeof(struct iphdr));
	/* Length of IP payload and header */
	iph->tot_len = htons(tx_len - sizeof(struct ether_header));
	
	/* Отправка пакета */
	/* Destination address */	
	struct sockaddr_ll socket_address;
	/* Index of the network device */
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	/* Destination MAC */
	for(int i = 0; i < LENMAC; i++) {
		socket_address.sll_addr[i] = macserver[i];
	}

	if (sendto(sockfd, sendbuf, tx_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0) {
	    printf("Send failed\n");
	}

	/* Принятие пакета от сервера */

	char buf[BUF_SIZ];

    int sockfroms;

    if ((sockfroms = socket(PF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1) {
        perror("listener: socket"); 
        return -1;
    }

    struct ifreq ifopts;

    strncpy(ifopts.ifr_name, DEFAULT_IF, IFNAMSIZ-1);
    ioctl(sockfroms, SIOCGIFFLAGS, &ifopts);

    ifopts.ifr_flags |= IFF_PROMISC;

    ioctl(sockfroms, SIOCSIFFLAGS, &ifopts);

    int sockopt;

    if (setsockopt(sockfroms, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) == -1) {
        perror("setsockopt");
        close(sockfroms);
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sockfroms, SOL_SOCKET, SO_BINDTODEVICE, DEFAULT_IF, IFNAMSIZ-1) == -1)  {
        perror("SO_BINDTODEVICE");
        close(sockfroms);
        exit(EXIT_FAILURE);
    }

    ssize_t numbytes;
    char sender[BUF_SIZ];

    while(1) {
    	printf("listener: Waiting to recvfrom...\n");
    	numbytes = recvfrom(sockfroms, buf, BUF_SIZ, 0, NULL, NULL);
    	printf("listener: got packet from server%zd bytes\n", numbytes);

    	int check = checkingMAC(buf, sockfroms);

	    if(check == 1) {
	    	struct sockaddr_storage their_addr;
	    	struct iphdr *iph = (struct iphdr *)(buf + sizeof(struct ether_header));   
	        ((struct sockaddr_in *)&their_addr)->sin_addr.s_addr = iph->saddr;
	    	inet_ntop(AF_INET, &(((struct sockaddr_in*)&their_addr)->sin_addr), sender, sizeof(sender));

	    	if(strcmp(sender, destIP) == 0) {
	    		infopacket(buf,numbytes);
	            close(sockfroms);
	            exit(0);
	        }
	    }
	    else {
            continue;
        } 
	}
  
	return 0;
}