#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>

struct  {
	int type;
	int value;
} mymsg;

int main() {
	int key;
	int msgqid;
	int bees = 3;
	int bear = 1;
	int pid[bees+bear];
	int status, stat;
	int portion_for_bee = 1;
	int portion_for_bear = 2;
	int barrel = 0;
	int bellyful = 0;
	int lim_bellyful = 3;
	int rc;

	key = ftok(".", 'm');
	if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
		perror("msgget");
		exit(1); 
	}

	for (int i = 0; i < bees+bear; i++) {
		while( bellyful < lim_bellyful ) {
			pid[i] = fork();
			srand(getpid());
			if(pid[i] == -1) {
				perror("fork");
				exit(0);
			}
			else {
				if (pid[i] == 0) {
					int k;
					k = rand()%(bees+bear);
					if (k == 0) {
						int delay;
						delay = rand()%5+1;
						sleep(delay);
						printf("Задержка: %d\n", delay);
						mymsg.value = portion_for_bear;
						mymsg.type = 1;
						msgsnd(msgqid, &mymsg, sizeof(mymsg), 0);
						exit(1);
					}
					else {
						int delay;
						delay = rand()%5+1;
						sleep(delay);
						printf("Задержка: %d\n", delay);
						mymsg.value = portion_for_bee;
						mymsg.type = 1;
						msgsnd(msgqid, &mymsg, sizeof(mymsg), 0);
						exit(2);
					}
				}
				else {
					status = waitpid(pid[i], &stat, 0);
					if (pid[i] == status) {
						if((WEXITSTATUS(stat)) == 1) {
							msgrcv(msgqid, &mymsg, sizeof(mymsg), 0, 0);
							printf("Медведь ест.\n");
							if(barrel < portion_for_bear) {
								bellyful-=(mymsg.value-barrel);
								bellyful+=barrel;
								barrel = 0;
								printf("Мед в бочке: %d\n", barrel);
								printf("Сытость: %d\n", bellyful);
							}
							else {
								barrel-=mymsg.value;
								bellyful+=mymsg.value;
								printf("Мед в бочке: %d\n", barrel);
								printf("Сытость: %d\n", bellyful);
							}
						}
						if(WEXITSTATUS(stat) == 2) {
							printf("Пчела завершила добычу.\n");
							msgrcv(msgqid, &mymsg, sizeof(mymsg), 0, 0);
							barrel+=mymsg.value;
							printf("Мед в бочке: %d\n", barrel);
						}
					}
				}
			}
		}
	}
	
	printf("Медведь сыт.\n");

	if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0) {								// Удаление очереди
		perror( strerror(errno) );
		printf("msgctl (return queue) failed, rc=%d\n", rc);
		return 1;
	}

	return 0;
}