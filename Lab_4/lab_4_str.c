#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <stdbool.h>

#define SIZE 256

int main(int argc, char* argv[]) {

	int ch;
	char s[] = "$";													//Cимвол для исключения строки
	char buf_str[SIZE];
	
	printf("Лабораторная работа №4\n\n");
	
	FILE *source, *out; 

	if((source = fopen(argv[1] , "r")) == NULL) {
		printf("Не удается открыть %s\n", argv[1] );
		exit(1);
	}
	out = fopen(argv[2], "w");

	while (fgets (buf_str, SIZE, source) != NULL) {
		printf("Чтение\n");
		int check = 0;
		for(int i = 0; i < strlen(buf_str); i++) {
			if (buf_str[i] == s[0]) {
				check++;
			}
		}
		if(check == 0) {
			fprintf(out, "%s", buf_str);
		}
	}
	
	fclose(out);
	fclose(source);

	return 0;
} 