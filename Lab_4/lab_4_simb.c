#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <stdbool.h>

#define SIZE 256

int main(int argc, char* argv[]) {

	int ch;
	char s[] = "$";														//Cимвол для замены
	// char name[] = "source_simb.txt";
	// argv[1] = name;

	printf("Лабораторная работа №4\n\n");
	
	FILE *source, *out;

	if((source = fopen(argv[1] , "r")) == NULL) {
		printf("Не удается открыть %s\n", argv[1] );
		exit(1);
	}

	out = fopen(argv[2], "w");

	while((ch = getc(source)) != EOF) {

		if(ch == s[0]) {
			printf("Замена\n");
			ch = ' ';
		}
		fputc(ch, out);
	}

	fclose(out);
	fclose(source);

	return 0;
} 