#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <signal.h>
#include <fcntl.h>

int main(int argc, char* argv[]){

	int fd[2];
	int number_of_workers;
	int status, stat;
	int gold = 1000;
	int portion = 100;
	int stash = 0;
	int delay;

	// printf("Number of workers ? - ");
	// scanf("%d", &number_of_workers);
	number_of_workers = atoi(argv[1]);
	pid_t pid[number_of_workers];

	pipe(fd);
	printf("Gold in mine: %d\n", gold);
	printf("Gold in stash: %d\n", stash);
	while(gold > 0) {
		for(int i = 0;  i < number_of_workers; i++) {
			pid[i] = fork();	
			if(gold <= 0) {
				exit(0);
			}
			if(-1 == pid[i]){
				perror("fork");
				exit(1);
			} 
			else { 
				if(pid[i] == 0) {
					close(fd[0]);
					write(fd[1], &portion, sizeof(int));
					exit(2);
				}
				delay = rand()%5+1;
				sleep(delay);
				status = waitpid(pid[i], &stat, WIFEXITED(status));
				if(pid[i] == status) {
					if(WEXITSTATUS(stat) == 0) {	
						exit(0);
					}
					if(WEXITSTATUS(stat) == 2) {
						close(fd[1]);
						read(fd[0], &portion, sizeof(int));
						gold-=portion;
						printf("Worker - %d\n", i);
						printf("Delay: %d\n", delay);
						if(gold > 0) {
							printf("Gold in mine: %d\n", gold);
						}
						else {
							printf("Mine is empty.\n");
						}
						stash += portion;
						printf("Gold in stash: %d\n", stash);
					}
				} 
			}
		}
	}

	return(0);
}