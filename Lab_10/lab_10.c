#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#define q 40

typedef struct {
	int row;
	int rowSize;
	int **mas;
} DATA;

void *threadFunc(void* );

int main(int argc, char* argv[]) {

	int N = atoi(argv[1]);
	int M[N];
	pthread_t threads[N];

	srand(getpid());
	
	int **data_mas;

	data_mas = (int**)malloc(sizeof(int)*N);

	for(int i = 0; i < N; i++) {
		M[i] = rand()%10+1;
		data_mas[i] = (int*)malloc(sizeof(int)*M[i]);
		for(int j = 0; j < M[i]; j++) {
			data_mas[i][j] = rand()%200+1;
		}
	}

	for(int i = 0; i < q; i++) {
		printf("=");
	}

	printf("\nMassive of data.\n");

	for(int i = 0; i < N; i++) {
		printf("%d). ", i);
		for(int j = 0; j < M[i]; j++) {
			printf("%d ", data_mas[i][j]);
		}
		printf("\n");
	}

	for(int i = 0; i < q; i++) {
		printf("=");
	}

	printf("\nAverage.\n");

	DATA *TrData = (DATA*)malloc(sizeof(DATA)*N);

	for(int i = 0; i < N; i++) {
		TrData[i].row = i;
		TrData[i].rowSize = M[i];
		TrData[i].mas = data_mas;
	}

	for (int i = 0; i < N; i++){
		pthread_create(&threads[i], NULL, threadFunc, &TrData[i]);
	}

	for (int i = 0; i < N; i++){
		pthread_join(threads[i], NULL);
	}

	for(int i = 0; i < q; i++) {
		printf("=");
	}

	printf("\n");

	free(TrData);

	for(int i = 0; i < N; i++) {
		free(data_mas[i]);
	}

	free(data_mas);

	return 0;
}

void *threadFunc(void *arg){

	DATA *pTrData = (DATA*)arg;
	int aver;
	int sum = 0;

	for(int i = 0; i < pTrData->rowSize; i++) {
		sum += pTrData->mas[pTrData->row][i];
	}

	aver = sum / pTrData->rowSize;
	printf("%d). Average: %d\n", pTrData->row,aver);
}