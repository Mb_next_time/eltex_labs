#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>

#define size_of_buf 256

typedef struct {														//Объявление структуры
	char* name_of_track;
	float length;
	int number_of_stop;
	float cost;
} track;

track* input(track* a, int q);										//Прототип для функции ввода
void display(track* a, int q);										//Прототип для функции вывода
int comp(const void* v2, const void* v1);							//Прототип для функции сравнения(qsort)

int main() {

	bool quit = false; 
	int N = 0;
	track* m = NULL;

	while(quit == false) {

		int choice;

		printf("Лабораторная работа №3\n\n");
		printf("1). Заполнение структуры(-р).\n");
		printf("2). Сортировать структуру(-ы).\n");
		printf("3). Вывод на экран.\n");
		printf("4). Выход.\n\n");
		printf("Выберите действие: ");
		scanf("%d", &choice);

		switch(choice) {
			case 1: {

				printf("Введите кол-во стуктур необходимых для заполнения: ");

				
				scanf("%d", &N);
				

				m = (track*)malloc(sizeof(track)*N);
				m = input(m, N);
				break;
			}
			case 2: {
				if (N == 0) {
					printf("Сортировать нечего, заполните структуру(-ы).\n\n");
				}
				else {
					qsort(m, N, sizeof(track), comp);
				}
				break;
			}
			case 3: {
				if (N == 0) {
					printf("Вывод недоступен, заполните для начала структуру(-ы).\n\n");
				}
				else {
					display(m, N);
				}
				break;
			}
			case 4: {
				free(m);
				printf("Выход >>> \n");
				quit = true;
				break;
			}
			default: {
				printf("Введите доступные действия!\n");
				break;
			}
		}
	}

	return 0;
}

track* input(track* a, int q) {											//Функция для заполнения структуры

	printf("Заполните структуру(ы).\n");

	for (int i = 0; i < q; i++) {

		char buf[size_of_buf];
		printf("Маршрут №%d\n", i+1);
		printf("Название маршрута: ");
		scanf(" %[^\n]", buf);
		buf[strlen(buf)] = '\0';
		a[i].name_of_track = (char*)malloc(sizeof(char)*strlen(buf));	
		strcpy(a[i].name_of_track, buf);

		printf("Введите протяженность маршрута: ");
		scanf("%f", &a[i].length);

		printf("Введите кол-во оcтановок маршрута: ");
		scanf("%d", &a[i].number_of_stop);

		printf("Введите стоимость маршрута: ");
		scanf("%f", &a[i].cost);
		printf("\n");	
	}

	return a;
}

void display(track* a, int q) {											//Функция для заполнения структуры

	printf("Вывод структур на экран: \n");

	for (int i = 0; i < q; i++) {

		printf("Маршрут №%d\n", i+1);
		printf("Название маршрута: %s\n", a[i].name_of_track);
		printf("Протяженность(в км): %f\n", a[i].length);
		printf("Кол-во остановок: %d\n", a[i].number_of_stop);
		printf("Стоимость: %f\n", a[i].cost);
	}
}

int comp (const void* v2, const void* v1)								//Функция для сравнения
{
	const track *m_i = (track*)v1;
	const track *m_j = (track*)v2;

	if (m_i->cost < m_j->cost) {
		return -1;
	}
	else { 
		if (m_i->cost > m_j->cost) {
			return +1;
		}
		else {
			return 0;
		}
	}
}
