#!/bin/bash
DIR=/home/nfs;
SHAREDIR=/mnt/nfs/home/Work/gameover;
FILE='192.168.101.147';
SERV_IP='192.168.101.167';

sudo mount -t nfs ${SERV_IP}:${DIR} ${SHAREDIR};

check=`mount -t nfs |grep ${SHAREDIR}`;
if [[ -n ${check} ]]; then
	echo "${SHAREDIR} уже монтирована!"
else
	echo "${SHAREDIR} не замонтирована! Монтируем ...";
	sudo mount -t nfs ${SERV_IP}:${DIR} ${SHAREDIR};
	sudo touch ${SHAREDIR}/${FILE};
	echo -n "Ushakov V.A." >> ${SHAREDIR}/${FILE};
	uptime >> ${SHAREDIR}/${FILE};
fi