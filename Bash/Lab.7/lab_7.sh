#!/bin/bash

SERVICES=/etc/services;
LIST=/home/gameover/Work/Bash/Lab.7/list_of_ports;

iptables -F;d
iptables -X;
iptables -P INPUT ACCEPT;
iptables -P FORWARD ACCEPT;
iptables -P OUTPUT ACCEPT;

# dpkg -l >> ${BUF};

lim=`cat ${LIST} | wc -l`;
i=1;

while [ "${i}" -le "${lim}" ]
do
	app=`awk 'NR=='$i' {printf $1}' ${LIST}`;
	# echo "${app}";
	com=`grep -i -w ${app} ${SERVICES}`; 
	if [[ -n ${com} ]]; then
		port=`awk 'NR=='$i' {printf $2}' ${LIST}`;
		# echo "ACCEPT";
		iptables -A INPUT -m state --state NEW,ESTABLISHED,RELATED -p tcp --dport ${port} -j DROP;
		iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -p tcp --dport ${port} -j DROP;
		let "i=i+1";
	else
		let "i=i+1";
	fi
done

iptables -A INPUT -j ACCEPT;
iptables -A OUTPUT -j ACCEPT;
