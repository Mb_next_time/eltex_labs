#будильник
#!/bin/bash
CRONUSER=/home/gameover/Work/Bash/Lab.2/gameover;
BUF=/home/gameover/Work/Bash/Lab.2/buf;
FILESOUND=/home/gameover/Work/Bash/Lab.2/Music_for_alarm/list;
quit=1;																				#Переменная для выхода из меню																			
check=1;	

while [ "$quit" -eq "$check" ]
do
	echo "Будильник.";
	echo "1. Добавить будильник.";
	echo "2. Просмотр будильников.";
	echo "3. Удалить будильник.";
	echo "4. Сбросить все настройки.";
	echo -e "5. Выход\n";

	echo -n "Выберите действие: ";
	read choice;

	case $choice in
		1)
		findcron=`find -name gameover`;
		#echo "$findcron";
		checkfile='./gameover';
		#echo "$check";
		if [ "$findcron" != "$checkfile" ]; then
			echo "SHELL=/bin/bash" >> ${CRONUSER};
			echo "DISPLAY=:0" >> ${CRONUSER};
		fi
		echo -n "Во сколько минут? - ";
		read m;
		echo -n "Во сколько часов? - ";
		read h;
		echo -n "В какой день недели? - ";
		read dow;
		lim=`cat $FILESOUND | wc -l`;															#подсчет кол-ва строк в файле
		i=1;
		count=1; 																															
		while [ "$i" -le "$lim" ]																
		do
			echo -n "$count). ";
			sed -n ${i}p $FILESOUND;															#вывод определенной строки i
			let "i=i+1"; 
			let "count=count+1";
		done
		echo "Выберите файл для проигрывания: ";
		read i;
		choice=`sed -n ${i}p $FILESOUND`;
		echo "$m $h * * $dow rhythmbox /home/gameover/Work/Bash/Lab.2/Music_for_alarm/$choice" >> ${CRONUSER};
		crontab ${CRONUSER};
		;;
		2)
		lim=`cat ${CRONUSER} | wc -l`;															#подсчет кол-ва строк в файле
		if [ "$lim" -le "2" ]; 
			then
			echo "Установленных будильников нет.";
		else
			i=3;
			count=1; 																															
			while [ "$i" -le "$lim" ]															#вывод определенных строк (с 3 по lim)
			do
				sed -n ${i}p ${CRONUSER} >> ${BUF};
				d_m=`awk 'NR == 1 {printf $1}' ${BUF}`;
				d_h=`awk 'NR == 1 {printf $2}' ${BUF}`;
				d_dow=`awk 'NR == 1 {printf $5}' ${BUF}`;
				echo "$count). ${d_h}:${d_m} - ${d_dow}";
				rm -f ${BUF};
				let "i=i+1"; 
				let "count=count+1";
			done
		fi
		;;
		3)
		lim=`cat ${CRONUSER} | wc -l`;															#подсчет кол-ва строк в файле
		if [ "$lim" -le 2 ]; 
			then
			echo "Установленных будильников нет.";
		else
			i=3;
			count=1; 																			#вывод определенных строк (с 3 по lim)													
			while [ "$i" -le "$lim" ]
			do
				sed -n ${i}p ${CRONUSER} >> ${BUF};
				d_m=`awk 'NR == 1 {printf $1}' ${BUF}`;
				d_h=`awk 'NR == 1 {printf $2}' ${BUF}`;
				d_dow=`awk 'NR == 1 {printf $5}' ${BUF}`;
				echo "$count). ${d_h}:${d_m} - ${d_dow}";
				rm -f $BUF;
				let "i=i+1";
				let "count=count+1"; 
			done
			echo "Какой будильник удалить?";
			rm -f $BUF;
			read i;
			let "i=i+2";
			sed ${i}d ${CRONUSER} >> ${BUF};														#удаление строки i
			rm -f ${CRONUSER};
			cat ${BUF} >> ${CRONUSER};
			rm -f ${BUF};
			crontab ${CRONUSER};
		fi
		;;
		4)
		crontab -r;
		rm -f ${CRONUSER};
		echo "Настройки сброшены.";
		;;
		5)
		echo "Выход >>> ";
		quit=0;
		exit;
		;;
	esac
done