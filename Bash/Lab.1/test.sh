#!/bin/bash

filename=buf;

declare -a array1

array1=( `cat "$filename" | tr '\n' ' '`)  # Загрузка содержимого файла
                                           # $filename в массив array1.
#         Вывод на stdout.
#                         с заменой символов перевода строки на пробелы.
element_count=${#array1[*]}
i=0;
while [ "$i" -lt "$element_count" ]
do
	printf "$i %s\n" "${array1[i]}"
	let "i=i+1";    
done
echo $element_count          # 8