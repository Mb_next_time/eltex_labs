#!/bin/bash

quit=1;																				#Переменная для выхода из меню																			
check=1;																			#Переменная для проверки 'quit'
declare -a list;
BUF=/home/gameover/Work/Bash/Lab.1/buf;

while [ "$quit" -eq "$check" ]
do
	rm -f /home/gameover/Work/Bash/Lab.1/buf;
	echo -e "Меню.";
	echo -n -e "Текущая директория: ";
	pwd;
	echo -e "\nСодержимое текущей директории.\n";
	echo -e "Директории: ";
	echo */ >> /home/gameover/Work/Bash/Lab.1/buf;									#Вывод списка директорий на экран
	list=( `cat "$BUF" | tr '\n' ' '`);													#Занесение элементов списка
	element_count=${#list[*]};															#Кол-во элементов списка
	i=0;
	index=1;																		#end
	while [ "$i" -lt "$element_count" ]										
	do
		printf "$index. %s\n" "${list[i]}";
		let "i=i+1"; 
		let "index=index+1";   
	done
	rm -f /home/gameover/Work/Bash/Lab.1/buf;
	echo -e;	
	echo -e "Файлы: ";																#Вывод списка файлов на экран
	find * -maxdepth 0 -type f >> /home/gameover/Work/Bash/Lab.1/buf;
	list=( `cat "$BUF" | tr '\n' ' '`);													#Занесение элементов списка
	element_count=${#list[*]};															#Кол-во элементов списка
	i=0;
	index=1;		
	while [ "$i" -lt "$element_count" ]
	do
		printf "$index. %s\n" "${list[i]}";
		let "i=i+1"; 
		let "index=index+1";   
	done																			#end
	echo -e;
	echo "1. Создать поддиректорию или файл.";
	echo "2. Перейти в директорию.";
	echo "3. Удаление поддиректории или файла.";
	echo -e "4. Выход.\n";
	echo -n "Выберите команду: ";

	read choice;																	#Переменная выбора ур 0

	case $choice in																	
		1)
		echo -e "Вы выбрали команду 1.\n";
		quit_1=1;
		while [ "$quit_1" -eq "$check" ]
		do 	
		rm -f /home/gameover/Work/Bash/Lab.1/buf;
		echo "1. Создать поддиректорию.";
		echo "2. Создать файл.";
		echo "3. Возврат в основное меню.";
		echo -n "Выберите команду: ";
		read choice_1;															#Переменная выбора ур 1
		case $choice_1 in
			1)																	#Меню 1.1. Создание поддиректории в текущей директории
			echo -n "Введите имя поддиректории: ";		
			read namedir;
			mkdir $namedir;
			;;
			2)																	#Меню 1.2. Создание файла в текущей директории
			echo -n "Введите имя файла: ";
			read namefile;	
			touch $namefile;
			;;
			3)																	#Меню 1.3. Выход в основное меню
			echo -e "Возврат в основное меню.\n";
			rm -f /home/gameover/Work/Bash/Lab.1/buf
			quit_1=0;
			;;
			*) 
			echo -e "Введены недопустимые действия.\n"
			;;
		esac
		done
		;;
		2)																			#Меню 2.1. Переход между директориями
		echo -e "Вы выбрали команду 2.\n";
		echo -n "Пропишите полный путь до директории: ";
		read namedir;
		cd $namedir;
		echo "$namedir";
		echo -e "Возврат в основное меню.\n";
		;;
		3)																			#Меню 3.1. Удаление поддиректории в текущей директории
		echo -e "Вы выбрали команду 3.\n";
		quit_1=1;
		while [ "$quit_1" -eq "$check" ]
		do 	
			rm -f /home/gameover/Work/Bash/Lab.1/buf;
			echo "1. Удалить поддиректорию.";
			echo "2. Удалить файл.";
			echo "3. Возврат в основное меню.";
			echo -n "Выберите команду: ";
			read choice_1;															#Переменная выбора ур 1
			case $choice_1 in
				1)																	#Меню 1.1. Удаление поддиректории в текущей директории
				echo -n "Введите имя поддиректории для удаления: ";		
				read namedir;
				rm -rf $namedir;
				;;
				2)																	#Меню 1.2. Удаление файла в текущей директории
				echo -n "Введите имя файла для удаления: ";
				read namefile;	
				rm -f $namefile;
				;;
				3)																	#Меню 1.3. Выход в основное меню
				echo -e "Возврат в основное меню.\n";
				rm -f /home/gameover/Work/Bash/Lab.1/buf
				quit_1=0;
				;;
				*) 
				echo -e "Введены недопустимые действия.\n"
				;;
			esac
		done
		echo -e "Возврат в основное меню.\n";
		;;
		4)																			#Меню 4.1. Выход
		echo "Выход >>> ";
		rm -f /home/gameover/Work/Bash/Lab.1/buf
		quit=0;
		;;
		*) 
		echo -e "Введены недопустимые действия.\n"
		;;
	esac;
done;
exit 0;