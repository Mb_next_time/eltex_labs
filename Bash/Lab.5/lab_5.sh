#!/bin/bash
FOLDERLOG=/var/log/;
check=1;
quit=1;
nameproc="null";

while [ "${quit}" -eq "${check}" ]
do
	echo "Поиск лог-файлов процесса.";
	echo -n "1. Задать процесс для поиска.";
	if [ "${nameproc}" == "null" ]; then 
		echo "                 |";	
	else
		echo "                 | Ищем log-файлы для ${nameproc}.";
	fi
	echo "2. Поиск log-файлов для процесса.";
	echo -e "3. Выход.\n";	
	echo -n "Выберите действие: ";
	read choice;
	case $choice in
		1)
		echo  -n "Введите имя искомого процесса: ";
		read nameproc;
		;;
		2)
		if [ "${nameproc}" == "null" ]; then 
			echo -e "Задайте процесс для поиска log-файлов.\n";
		else
			echo "Задайте дату в формате \"year-month-day\" за которую вы хотите получить log-файлы или";
			echo -n "\"Enter\" для вывода всех log-файлов процесса за все время: ";
			read data;
			cd ${FOLDERLOG};
			tmp=`grep -r ${data}.*${nameproc} 2>/dev/null`;
			if [[ !(${tmp}) ]]; then
				echo -e "log-файлов для ${nameproc} не найдено, повторите ввод снова.\n";
			else
				grep -r ${data}.*${nameproc} 2>/dev/null;
			fi
		fi
		;;
		3)
		echo "Выход >>> ";
		quit=0;
		exit;
		;;	
	esac
done