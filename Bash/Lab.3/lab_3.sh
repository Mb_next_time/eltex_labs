#!/bin/bash 

quit=1;																				#Переменная для выхода из меню																			
check=1;

while [ "$quit" -eq "$check" ]
do
	echo "Git.";
	echo "1. Создание репозитория.";
	echo "2. Узнать статус.";
	echo "3. Добавление файлов к репозиторию.";
	echo "4. Отправка на удаленный репозиторий.";
	echo "5. Фиксация изменений.";
	echo "6. Создание новой ветки.";
	echo "7. Вывод списка изменений.";
	echo "8. Получение файлов с удаленного репозитория.";
	echo "9. Клонирование репозитория.";
	echo -e "10. Выход.\n";
	echo -n "Выберите действие: ";

	read choice;

	case $choice in
		1)
		git init;
		echo -n "Укажите ссылку на удаленный репозиторий: ";
		read namerep;
		git remote add origin ${namerep};
		;;
		2)
		git status;
		;;
		3)
		echo "Список файлов.";
		ls;
		echo "Укажите имя файла для добавления на удаленный репозиторий." 
		read namefile;
		git add ${namefile};
		;;
		4)
		git push -u origin master;
		;;
		5)
		echo -n "Введите коммит: ";
		read namecom;
		git commit -m "${namecom}";
		;;
		6)
		echo -n "Введите название ветки: ";
		read namebranch;
		git checkout -b ${namebranch};
		;;
		7)
		git log;
		;;
		8)
		;;
		9)
		echo "Укажите ссылку на репозиторий для клонирования.";
		read namerepcl;
		git clone ${namerepcl};
		;;
		10)
		echo "Выход >>> ";
		quit=0;
		exit;
		;;
	esac
done




