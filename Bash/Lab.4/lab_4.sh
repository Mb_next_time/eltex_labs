#!/bin/bash
CRONUSER=/home/gameover/Work/Bash/Lab.4/gameover.txt;
BUF=/home/gameover/Work/Bash/Lab.4/buf.txt;
SCRIPT=/home/gameover/Work/Bash/Lab.4/lab_4.sh;
LOG=/home/gameover/Work/Bash/Lab.4/log.txt;

quit=1;																												#Переменная для выхода из меню																			
check=1;
status=$1;
if [[ -n $status ]]; then
	# echo "LOG1" >> ${LOG}; 
	while [ "${quit}" -eq "${check}" ]
	do
		echo "Контроль процессов.";
		echo "1. Добавить процесс в контроль.";
		echo "2. Просмотр процессов, находящихся в контроле.";
		echo "3. Убрать процесс из контроля.";
		echo "4. Сброс.";
		echo -e "5. Выход\n";

		echo -n "Выберите действие: ";
		read choice;

		case $choice in
			1)
			findcron=`find -name gameover.txt`;
			echo "${findcron}";
			checkfile='${CRONUSER}';
			echo "${check}";
			echo -n "Задайте имя процесса: "
			read nameprocess;
			# nameprocess='chromium-browser';
			if [ "${findcron}" != "${checkfile}" ]; then
				echo "SHELL=/bin/bash" >> ${CRONUSER};
				echo "DISPLAY=:0" >> ${CRONUSER};
			fi
			echo "Задайте время проверки процесса.";
			echo "Минуты от 0 до 59 или период, используя /*минуты:"
			echo -n "Минуты: ";
			read m;
			echo "Часы от 0 до 23 или период, используя */часы:"
			echo -n "Часы: ";
			read h;
			echo "${m} ${h} * * * ${SCRIPT} >/dev/null 2>&1 & #control ${nameprocess} " >> ${CRONUSER};

			echo "${m} ${h} ${nameprocess}" >> ${LOG};
			crontab ${CRONUSER};
			;;
			2)
			grep "control" ${CRONUSER} >> ${BUF};
			awk '{ print NR, $2, $1, $11 }' ${BUF};
			rm -f ${BUF};
			;;																													
			3)
			grep "control" ${CRONUSER} >> ${BUF};
			awk '{ print NR, $2, $1, $11 }' ${BUF};
			# rm -f ${BUF};
			echo -n "Какой процесс убрать из контроля? - ";
			read i;
			str=`sed -n ${i}p ${BUF}`;
			rm -f ${BUF};
			echo "${str}" >> ${BUF};
			m_p=`awk 'NR == 1 {printf $1}' ${BUF}`;
			h_p=`awk 'NR == 1 {printf $2}' ${BUF}`;
			n_p=`awk 'NR == 1 {printf $11}' ${BUF}`;
			# echo "${m_p}: ${h_p} ${n_p}" >> ${LOG};
			rm -f ${BUF};
			grep -v "${m_p}.${h_p}.*${n_p}" ${CRONUSER} >> ${BUF};
			rm -f ${CRONUSER};
			cat ${BUF} >> ${CRONUSER};
			rm -f ${BUF};
			crontab ${CRONUSER};
			;;
			4)
			crontab -r;
			rm -f ${BUF};
			rm -f ${CRONUSER};
			rm -f ${LOG};
			;;		
			5)
			echo "Выход >>> ";
			quit=0;
			exit;														
			;;
		esac
	done
else
	i=1;
	lim=`cat ${LOG} | wc -l`;
	while [ "$i" -le "$lim" ]															#вывод определенных строк (с 3 по lim)
		do
		sed -n ${i}p ${LOG} >> ${BUF};
		m_p=`awk 'NR == 1 {printf $1}' ${BUF}`;
		h_p=`awk 'NR == 1 {printf $2}' ${BUF}`;
		n_p=`awk 'NR == 1 {printf $3}' ${BUF}`;
		finding=`grep -e "${m_p}.${h_p}.*${n_p}" ${CRONUSER}`;
		if [[ -n $finding ]]; then
			tmp=`pgrep "${n_p}"`;
			if [[ !(${tmp}) ]]; then
				(${n_p} >/dev/null 2>&1 &);
			fi
		fi
		rm -f ${BUF};
		let "i=i+1"; 
	done
fi
